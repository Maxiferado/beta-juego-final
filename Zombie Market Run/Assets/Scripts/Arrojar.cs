﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrojar : MonoBehaviour {


    Transform mTransform;
    Transform posicionReferencia;
    ControlJugador mJugador;

	// Use this for initialization
	void Start () {

        mTransform = GetComponent<Transform>();
        posicionReferencia = GameObject.Find("Referencia").GetComponent<Transform>();
        mJugador = GetComponent<ControlJugador>();
		
	}
	
	// Update is called once per frame
	void Update () {

        if(mJugador.cantidadDistractores >= 1)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                Disparar();
                mJugador.cantidadDistractores--;
            }
        }
        
		
	}
    public void Disparar()
    {
        GameObject distractor = Pool.Piscina.SacarDeLaPool(); //Distractor es el objeto que representa en este script el gameobject que devuelve la garra. 
        if(distractor != null)
        {
            distractor.transform.position =  posicionReferencia.position; //El objeto está en alguna parte del mapa. Lo vamos a traer cerca del jugador. esto es como otra forma de punto de referencia
            distractor.transform.rotation = Quaternion.identity;
            distractor.SetActive(true);
            Rigidbody cuerpoDistractor = distractor.GetComponent<Rigidbody>();
            cuerpoDistractor.AddForce(1000 * mTransform.forward * 1);

        }

    }
}
