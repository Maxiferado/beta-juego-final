﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambioDeEscena : MonoBehaviour {

    public int Jugadores;
    public int Creditos;
    public int Menu;
    public int Nivel1;
    public int Nivel2;
    public int Nivel3;

    public void LoadScene(string _SceneName) {
        SceneManager.LoadScene(_SceneName);
    }

    public void CambioJugador()
    {
        SceneManager.LoadScene(Jugadores);
    }

    public void Exit()
    {
        Application.Quit();
        Debug.Log("Exit");
    }

    public void CambioCreditos()
    {
        SceneManager.LoadScene(Creditos);
    }

    public void CambioMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void CambioNivel1()
    {
        SceneManager.LoadScene(Nivel1);
    }

    public void CambioNivel2()
    {
        SceneManager.LoadScene(Nivel2);
    }

    public void CambioNivel3()
    {
        SceneManager.LoadScene(Nivel3);
    }

    public void Tienda()
    {

        SceneManager.LoadScene("Tienda");
    }

    public void Instruccion()
    {

        SceneManager.LoadScene("Instrucciones");
    }

    public void Limpiar()
    {
        PlayerPrefs.SetInt("Nivel", 1);
        Debug.Log("Yes");
        CambioMenu();
    }

}
