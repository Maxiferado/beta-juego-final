﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carrito : MonoBehaviour {
    
    MeshRenderer mRender;     
    Transform jugador;    
    public float acumulado = 0;   
    Movimiento mMovimiento;
    float llenoCon;
 


    // Use this for initialization
    void Start() {
        mMovimiento = GetComponentInParent<Movimiento>();
        mRender = GetComponent<MeshRenderer>();
        jugador = GameObject.Find("Jugador").GetComponent<Transform>();
        transform.parent = null;
        Debug.Log("Se llena con " + llenoCon);
        //cuerpo.isKinematic = true;
    }

    // Update is called once per frame
    void Update() {

        if (mMovimiento.hijo == true)
        {
            if (Input.GetKey(KeyCode.E))
            {
                transform.parent = null;
                Debug.Log("No soy hijo");
                mMovimiento.hijo = false;
                //cuerpo.isKinematic = true;

            }
        }
        
    }
   
    public void OnCollisionStay(Collision pegadito)
    {
        if(pegadito.gameObject.tag == "Jugador")
        {
            if (Input.GetKey(KeyCode.C))
            {
                transform.parent = jugador.transform;
                Debug.Log("Soy hijo");
                mMovimiento.hijo = true;
                //cuerpo.isKinematic = false;
            }
            
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.parent = null;
            Debug.Log("No soy hijo");
            //cuerpo.isKinematic = true;

        }
    }




}
