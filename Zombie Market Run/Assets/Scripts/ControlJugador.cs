﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
[RequireComponent(typeof(Renderer))]

public class ControlJugador : MonoBehaviour, IDestruccion {

    Movimiento mMovimiento;
    Slider laUtilidad;
    Coleccion laColeccion;
    public int GameOver;
    public float t = 0;
    public float cantidadRecolectables = 0;
    public float cantidadDistractores = 0;
    public float vida = 3f;
    public float Salud = 100;
    Carrito mCarrito;
    Capturador mCapturador;
    GameObject Sangre;
    Renderer renderPlano;
    GameObject Palpitacion;
    public GameObject pildo;
    public GameObject adre;
    public GameObject ray;
    Transform elCarro;
    Transform mTransform;
    public Text[] conta = new Text[3];


    // Use this for initialization
    void Start()
    {
        mMovimiento = GetComponent<Movimiento>();
        laUtilidad = GetComponentInChildren<Slider>();
        laColeccion = GetComponent<Coleccion>();
        mCarrito = GameObject.Find("Carrito").GetComponent<Carrito>();
        mCapturador = GetComponent<Capturador>();
        Sangre = GameObject.FindGameObjectWithTag("Sangre");
        Palpitacion = GameObject.FindGameObjectWithTag("Palpitacion");
        renderPlano = GetComponentInChildren<Renderer>();
        elCarro = GameObject.Find("Carrito").GetComponent<Transform>();
        mTransform = GetComponent<Transform>();
        

        
    }

    // Update is called once per frame
    void Update()
    {
        CheckItems();

        t += Time.deltaTime;

        mMovimiento.Mover();

        if (Salud <= 0)
        {
            Muere();
        }
        if (t >= 300)
        {
            SceneManager.LoadScene(GameOver);
        }
        // Para el ruido sería hacer un collider trigger que vaya aumentando conforme el input --> Vector3 (1,1,1) x 10 x input

        if (Salud <= 30)
        {
            Sangre.SetActive(true);
        }
        else
        {
            Sangre.SetActive(false);
        }

        if (Salud <= 20 && Salud >= 15 || Salud <= 10)
        {
            Palpitacion.SetActive(true);
        }
        else
        {
            Palpitacion.SetActive(false);
        }
    }

    public void Destruir(float Cantidad)
    {
        Salud -= Cantidad;
    }

    public void Muere()
    {
        if (Salud <= 0)
        {
            vida--;
            Salud = 100;
        }
        if (vida == 0)
        {
            SceneManager.LoadScene(GameOver);
        }
    }

    private void OnCollisionEnter(Collision enElCarro)
    {

        if (enElCarro.gameObject.tag == "Carrito" && cantidadRecolectables >= 1f)
        {
            mCarrito.acumulado = cantidadRecolectables;
            cantidadRecolectables = 0;

        }
    }
    public void CheckItems()
    {

        int temp0 = PlayerPrefs.GetInt("Cpildora");
        Debug.Log("pildo");
        if (temp0 > 0)
        {
            Debug.Log("pildot");
            pildo.SetActive(true);
            
            conta[0].text = temp0.ToString();
        }
        int temp1 = PlayerPrefs.GetInt("Cadrenalina");
        Debug.Log("adre");
        if (temp1 > 0)
        {
            Debug.Log("adret");
            adre.SetActive(true);
            conta[1].text = temp1.ToString();
        }
        int temp2 = PlayerPrefs.GetInt("Crayo");
        Debug.Log("ray");
        if (temp2 > 0)
        {
            Debug.Log("rayt");
            ray.SetActive(true);
            conta[2].text = temp2.ToString();
        }
    }


    public void Pildora()
    {
        int tcant = PlayerPrefs.GetInt("Cpildora");
        tcant -= 1;
        if (tcant <= 0)
        {
            pildo.SetActive(false);
        }
        PlayerPrefs.SetInt("Cpildora", tcant);
        Salud += 50;
    }

    public void Adrenalina()
    {
        int tcant = PlayerPrefs.GetInt("Cadrenalina");
        tcant -= 1;
        if (tcant <= 0)
        {
            adre.SetActive(false);
        }
        PlayerPrefs.SetInt("Cadrenalina", tcant);
        mMovimiento.AddMagnitud(1);
        Salud += 20;

    }
    public void Rayo()
    {
        int tcant = PlayerPrefs.GetInt("Crayo");
        tcant -= 1;
        if (tcant <= 0)
        {
            ray.SetActive(false);
        }
        PlayerPrefs.SetInt("Crayo", tcant);
        mMovimiento.AddMagnitud(3);

    }



}