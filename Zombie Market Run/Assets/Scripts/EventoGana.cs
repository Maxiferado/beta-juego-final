﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EventoGana : MonoBehaviour {
    public delegate void Ganar();
    public static event Ganar EventoGano;
    Carrito mCarrito;
    float maximoAcumulado = 3;
    int Win = 7;
	// Use this for initialization
	void Start () {

        mCarrito = GetComponent<Carrito>();
        Debug.Log("Máximo Acumulado = " + maximoAcumulado);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void Gano()
    {
        if(mCarrito.acumulado == maximoAcumulado && EventoGano != null)
        {
            EventoGano();           
        }
    }
}
