﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movimiento : MonoBehaviour
{
  
    Slider mSlider;
    float Magnitud = 5;
    public float MagnitudAngular;
    Transform mTransform;
    AudioSource mAudio, audioCarrito;
    public float masterVolume=0;
    public bool meDetectan = false;
    public bool hijo = false;
    Carrito elCarro;
    public float tiempo = 0;

   
 

    // Use this for initialization
    void Start()
    {
        mSlider = GetComponentInChildren<Slider>();
        mTransform = GetComponent<Transform>();
        mAudio = GetComponent<AudioSource>();
        audioCarrito = GameObject.Find("Carrito").GetComponentInChildren<AudioSource>();
        elCarro = GameObject.Find("Carrito").GetComponent<Carrito>();
        mAudio.volume = 0.2f;
        audioCarrito.volume = 0;
        masterVolume = mAudio.volume+audioCarrito.volume;
     
    }

    // Update is called once per frame
    void Update()
    {
        mSlider.value = masterVolume;
        if (masterVolume>=0.8)
        {
            meDetectan = true;
        }
        if(masterVolume<=0.8)
        {
            meDetectan = false;
        }
        if (meDetectan)
        {
            masterVolume = masterVolume * 1;//Esto es para que cuando camine, el audio continue sobrepasando 0.8 (el volumen de la caminata es más bajito)
        }
        if (!meDetectan)
        {
            masterVolume = mAudio.volume + audioCarrito.volume;
        }
        if (masterVolume>=1)
        {
            masterVolume = masterVolume * 1;
        }
        if(meDetectan)
        {
            tiempo += Time.deltaTime;
            if (tiempo >= 3)
            {
                meDetectan = false;
                tiempo = 0;
                masterVolume = mAudio.volume + audioCarrito.volume;
            }
        }
        if (Input.GetKey(KeyCode.Space))
        {
            Magnitud = 7;
            if (masterVolume <= 1)
            {
                print("subo Volumenes");
                mAudio.volume += 0.005f;
                if (hijo==true) { audioCarrito.volume += 0.005f; }
                if (hijo==false) { audioCarrito.volume = 0; }
                if (masterVolume >= 1)
                {
                    mAudio.volume = mAudio.volume * 1;
                    audioCarrito.volume = audioCarrito.volume * 1;
                }
            }
        }
        else
        {
            Magnitud = 5;
            mAudio.volume = 0.2f;
            audioCarrito.volume = 0.2f;
        }
    }

    public void Mover()
    {

        Vector3 direccionX = new Vector3(1, 0, 0);
        Vector3 direccionZ = mTransform.forward;
        Vector3 direccionY = new Vector3(0, 1, 0);

        float sentidoZ = Input.GetAxis("Vertical");
        float sentidoY = Input.GetAxis("Horizontal");
        //Debug.Log(sentidoZ);
        
        Vector3 velocidadAngular = MagnitudAngular * direccionY * sentidoY;
        Vector3 desplazamientoAngular = velocidadAngular * Time.deltaTime;
        mTransform.eulerAngles += desplazamientoAngular;

        Vector3 velocidad = Magnitud * (direccionZ * sentidoZ);
        Vector3 desplazamiento = velocidad * Time.deltaTime;
        mTransform.position += desplazamiento;

       

        if (sentidoY != 0)
        {
            mAudio.mute = false;
        }
        else
        {
            mAudio.mute = true;
        }
        if (sentidoZ != 0)
        {
            mAudio.mute = false;
        }   
    }
    public void Detectado()
    {
        meDetectan = true;
    }
    public void AddMagnitud(int n)
    {
        Magnitud += n;
    }
}
