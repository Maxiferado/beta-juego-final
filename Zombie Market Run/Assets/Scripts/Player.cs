﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Player : MonoBehaviour {

    public GameObject inventario;
    public GameObject Tienda;
    public MouseLook mouseLook;
    public int Money;
    private int verificar;

    void Start()
    {
        verificar = PlayerPrefs.GetInt("firstTime");
        if (verificar == 0)
        {
            PlayerPrefs.SetInt("Money", 500);
            PlayerPrefs.SetInt("firstTime", verificar + 1);
        }
        
            //mouseLook = gameObject.GetComponent<FirstPersonController>().m_MouseLook;
    }

    void Update () {
        Money = PlayerPrefs.GetInt("Money");
        if (Input.GetKeyDown(KeyCode.I))
        {
            inventario.SetActive(!inventario.activeInHierarchy);
            inventario.transform.parent.transform.position = inventario.GetComponent<InventarioNuevo>().originalPos;
        }

        if (inventario.activeInHierarchy || Tienda.activeInHierarchy)
        {
            mouseLook.SetCursorLock(false);
        }
        else
        {
            mouseLook.SetCursorLock(true);
        }

        if (!inventario.activeInHierarchy && inventario.GetComponent<InventarioNuevo>().objetoSeleccionado != null)
        {
            inventario.GetComponent<InventarioNuevo>().objetoSeleccionado.gameObject.transform.SetParent(inventario.GetComponent<InventarioNuevo>().ExParent);
            inventario.GetComponent<InventarioNuevo>().objetoSeleccionado.gameObject.transform.localPosition = Vector3.zero;
            inventario.GetComponent<InventarioNuevo>().objetoSeleccionado = null;
        }
    }
}
