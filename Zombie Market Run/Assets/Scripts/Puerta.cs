﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Puerta : MonoBehaviour {

    Carrito carrito;
    public int Win;
    BoxCollider mCollider;
    AudioSource mAudio;
    Coleccion coleccion;
    

	// Use this for initialization
	void Start () {
        carrito = GetComponent<Carrito>();
        EventoGana.EventoGano += MeAbro;
        mCollider = GetComponent<BoxCollider>();
        mAudio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
       
	}

   private void OnCollisionEnter (Collision colision)
    {
        int acumulable = PlayerPrefs.GetInt("Acumulable");

        Debug.Log("Entro a la colisión");
        //SceneManager.LoadScene(Win);
        if (acumulable >= 3f)
        {
            PlayerPrefs.SetInt("Acumulable", 0);
            int money = PlayerPrefs.GetInt("Money");
            PlayerPrefs.SetInt("Money", money + 300);
            if (PlayerPrefs.GetInt("Nivel")== 2)
            {
                SceneManager.LoadScene("Win");
            }
            else
            {
                PlayerPrefs.SetInt("Nivel", 2);
                SceneManager.LoadScene("Win 1");
                
            }          

        }
    }

    public void MeAbro()
    {
        Debug.Log("Debería abrirse la puerta");
    }

    public void Suena()
    {
        mAudio.Play();
    }
    
}


