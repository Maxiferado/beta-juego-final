﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recuperar : MonoBehaviour {

    Memento[] mementos = new Memento[100];
    Transform mTransform;
    ControlJugador m_ControlJugador;
    int indice = 0;


    private void Start()
    {
        mTransform = GetComponent<Transform>();
        m_ControlJugador = GetComponent<ControlJugador>();
        Almacenar();
    }

    private void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.G))
        {
            Almacenar();
        }*/

        /*if (Input.GetKeyDown(KeyCode.T))
        {
            Cargar(indice);
        }*/
        //Debug.Log("El Update sí funciona");

        if (m_ControlJugador.Salud <= 0 && m_ControlJugador.vida != 0)
        {
            Debug.Log("Por lo que la salud es igual a:" + m_ControlJugador.Salud + "La vida actual con la que se reinicia el nivel es:" + m_ControlJugador.vida);
            Cargar(indice);
            //m_ControlJugador.Salud = 100;
        }
    }

    public void Almacenar()
    {
        Vector3 pos = mTransform.position;
        Vector3 rot = mTransform.eulerAngles;     

        mementos[indice] = new Memento(pos, rot);
        indice++;

        Debug.Log("Guardado :" + pos + rot );
        
    }

    public void Cargar(int _indice)
    {
        Vector3 pos = mTransform.position;
        Vector3 rot = mTransform.eulerAngles;       

        mTransform.position = mementos[_indice - 1].pos;
        mTransform.eulerAngles = mementos[_indice - 1].rot;
    }
}
